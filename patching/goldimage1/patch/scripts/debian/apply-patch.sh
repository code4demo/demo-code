#!/bin/bash

#Packages list declaration
set -o allexport
. scripts/.env
set +o allexport

TARGET=fixed
cd $TARGET
echo "================patching in progress======================"
if sudo dpkg --force-all -i *; then
   echo "================patching completed======================"
else
   echo "================patching failed======================"
   ../scripts/$PACKAGE_MANAGER_TYPE/rollback.sh
fi