#!/bin/bash

FILE=out.txt
FILE_FIXED=outfixed.txt
TARGET=backup

dobackup() {
  sed -i '/^Fetched\b/d' $FILE
  awk '{ print $5 }' $FILE > $FILE_FIXED
  FILE_DATA=`cat $FILE_FIXED`
  cd $TARGET
  echo "================backup in progress======================"
  for pkg in $FILE_DATA
  do
     echo "packing package -> $pkg"
     sudo dpkg-query -W $pkg | awk '{print $1}' | sudo xargs dpkg-repack
  done  
  echo "================backup completed======================"
}

dobackup